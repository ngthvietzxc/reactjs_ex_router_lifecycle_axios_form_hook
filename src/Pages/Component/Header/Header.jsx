import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export default class Header extends Component {
  routeArr = [
    {
      path: "/home",
      title: "Home",
    },
    {
      path: "/login",
      title: "Login",
    },
    {
      path: "/life-cycle",
      title: "Life Cycle",
    },
    {
      path: "/learn-hook",
      title: "Hook ",
    },
    {
      path: "/tai-xiu",
      title: "Game Tài Xỉu ",
    },
    {
      path: "/effect-page",
      title: "Use Effect ",
    },
  ];
  render() {
    return (
      <div className="bg-gray-800 text-gray-200">
        <div className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
          <div className="relative flex h-16 items-center justify-between">
            <div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
              <div className="hidden sm:ml-6 sm:block">
                <div className="flex space-x-4">
                  {this.routeArr.map((route, index) => {
                    return (
                      <NavLink
                        key={index}
                        className="text-gray-300 hover:bg-gray-700 hover:text-white rounded-md px-3 py-2 text-sm font-medium"
                        to={route.path}
                      >
                        {route.title}
                      </NavLink>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
