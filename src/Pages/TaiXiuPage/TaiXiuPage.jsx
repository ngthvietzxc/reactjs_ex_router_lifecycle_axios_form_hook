import React, { useState } from "react";
import bg_game from "../../assets/bgGame.png";
import "./game.css";
import XucXac from "./XucXac";
import KetQua from "./KetQua";

export const TAI = "TÀI";
export const XIU = "XỈU";

export default function TaiXiuPage() {
  const [xucXacArr, setXucXacArr] = useState([
    { img: "./imgXucXac/1.png", giaTri: 1 },
    { img: "./imgXucXac/2.png", giaTri: 2 },
    { img: "./imgXucXac/3.png", giaTri: 3 },
  ]);
  const [luaChon, setLuaChon] = useState(null);
  const [soBanThang, setSoBanThang] = useState(0);
  const [soLuotChoi, setSoLuotChoi] = useState(0);

  const handleDatCuoc = (value) => {
    setLuaChon(value);
  };
  const handlePlayGame = () => {
    console.log("Yes");
    let tongDiem = 0;
    let newXucXacArr = xucXacArr.map(() => {
      let number = Math.floor(Math.random() * 6) + 1;
      tongDiem += number;
      return {
        img: `./imgXucXac/${number}.png`,
        giaTri: number,
      };
    });
    setXucXacArr(newXucXacArr);
    setSoLuotChoi(soLuotChoi + 1);
    tongDiem >= 11 && luaChon === TAI && setSoBanThang(soBanThang + 1);
    tongDiem <= 11 && luaChon === XIU && setSoBanThang(soBanThang + 1);
  };
  return (
    <div
      className="game_container p-5"
      style={{ backgroundImage: `url(${bg_game})` }}
    >
      <div>
        <XucXac handleDatCuoc={handleDatCuoc} xucXacArr={xucXacArr} />
      </div>
      <KetQua
        soBanThang={soBanThang}
        soLuotChoi={soLuotChoi}
        handlePlayGame={handlePlayGame}
        luaChon={luaChon}
      />
    </div>
  );
}
