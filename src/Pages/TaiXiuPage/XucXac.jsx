import React from "react";
import { TAI, XIU } from "./TaiXiuPage";

export default function XucXac({ xucXacArr, handleDatCuoc, luaChon }) {
  const renderXucXacArr = () => {
    return xucXacArr.map((item, index) => {
      return (
        <img
          style={{ height: 100, margin: 10, display: "inline" }}
          src={item.img}
          key={index}
        />
      );
    });
  };
  return (
    <div className=" p-5 d-flex justify-content-between container">
      <button
        onClick={() => {
          handleDatCuoc(XIU);
        }}
        className="p-5 bg-red-400 hover:bg-red-600 text-gray-800 font-bold py-2 px-4 rounded"
      >
        Xỉu
      </button>
      <div>{renderXucXacArr()}</div>
      <button
        onClick={() => {
          handleDatCuoc(TAI);
        }}
        className="p-5 bg-red-400 hover:bg-red-600 text-gray-800 font-bold py-2 px-4 rounded"
      >
        Tài
      </button>
    </div>
  );
}
