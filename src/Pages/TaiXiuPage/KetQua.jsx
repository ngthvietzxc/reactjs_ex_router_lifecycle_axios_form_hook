import React from "react";

export default function KetQua({
  luaChon,
  handlePlayGame,
  soBanThang,
  soLuotChoi,
}) {
  return (
    <div className="p-5">
      <button
        onClick={handlePlayGame}
        className="border-2 tracking-wide hover:bg-blue-600 text-gray-800 font-bold py-2 px-4 rounded"
      >
        Play Game
      </button>
      <h1 className="tracking-wide p-4 text-3xl">Bạn chọn : {luaChon}</h1>
      <h1 className="tracking-wide p-4 text-2xl">
        Số bàn thắng : {soBanThang}
      </h1>
      <h1 className="tracking-wide p-4 text-2xl">
        Số lượt chơi : {soLuotChoi}
      </h1>
    </div>
  );
}
