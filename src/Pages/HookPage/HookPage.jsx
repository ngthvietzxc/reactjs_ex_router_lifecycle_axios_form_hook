import React, { useState } from "react";
import Checkout from "./Checkout";
// KHÔNG DÙNG THIS
export default function HookPage() {
  let [number, setNumber] = useState(1);
  console.log("🚀 ~ HookPage ~ number:", number);
  let handleTang = () => {
    setNumber(number + 1);
  };
  let handleGiam = () => {
    setNumber(number - 1);
  };
  let handleThanhToan = () => {
    setNumber(0);
  };
  return (
    <div>
      <div
        style={{ width: 220, margin: "0 auto" }}
        className="pl-4 pt-4 pb-4 flex"
      >
        <button
          onClick={handleGiam}
          className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l"
        >
          -
        </button>
        <button className=" font-bold py-2 px-4 rounded-r">{number}</button>
        <button
          onClick={handleTang}
          className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r"
        >
          +
        </button>
      </div>
      <Checkout number={number} handleThanhToan={handleThanhToan} />
    </div>
  );
}
