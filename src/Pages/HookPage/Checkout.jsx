import React from "react";

export default function Checkout(props) {
  console.log("🚀 ~ Checkout ~ props:", props);
  return (
    <div className="p-5 bg-red-600  ">
      <h2 className="text-3xl font-bold">Checkout</h2>
      <p className="text-2xl font-bold pb-4">
        Total money: {props.number * 100} $
      </p>
      <button
        onClick={props.handleThanhToan}
        className="bg-yellow-300 hover:bg-yellow-500 text-gray-800 font-bold py-2 px-4 rounded"
      >
        Pay Now
      </button>
    </div>
  );
}
