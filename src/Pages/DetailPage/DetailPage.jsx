import React, { Component } from "react";
import { withRouter } from "../../HOC/withRouter";
import axios from "axios";
import { Card } from "antd";

const { Meta } = Card;

class DetailPage extends Component {
  state = { movie: {} };
  componentDidMount() {
    let { id } = this.props.router.params;
    axios({
      url: `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
      method: "GET",
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNiIsIkhldEhhblN0cmluZyI6IjExLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDM5MDQwMDAwMCIsIm5iZiI6MTY3MDYwNTIwMCwiZXhwIjoxNjk0NTM4MDAwfQ.ndRbsF4IYaA6syfyNt9AVnZnm1kn9MAHUKKo4rXHLVE",
      },
    })
      .then((res) => {
        console.log(res);
        this.setState({ movie: res.data.content });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    console.log(this.props);
    return (
      <div>
        <Card
          hoverable
          style={{
            width: 240,
          }}
          cover={<img alt="example" src={this.state.movie.hinhAnh} />}
        >
          <Meta
            title={this.state.movie.tenPhim}
            description="www.instagram.com"
          />
        </Card>
      </div>
    );
  }
}
export default withRouter(DetailPage);

// antd npm
