import React, { Component } from "react";
import Nav from "./Nav";

export default class LifeCyclePage extends Component {
  state = { number: 1 };
  componentDidMount() {
    console.log("Parent-Didmount");
  }
  handleIncrease = () => this.setState({ number: this.state.number + 1 });
  handleDecrease = () => this.setState({ number: this.state.number - 1 });
  shouldComponentUpdate(nextProps, nextState) {
    // return true => render lại, return false => ko render
    if (nextState.number === 5) {
      return false;
    } else {
      return true;
    }
  }
  render() {
    console.log("Parent-Rerender");
    return (
      <div>
        <h1 className="p-4 text-black-800 mb-8 text-4xl font-bold sm:text-5xl md:mb-12 md:text-6xl">
          Life Cycle Page
        </h1>

        <div style={{ width: 200, margin: "0 auto" }} className="pl-4 flex">
          <button
            onClick={this.handleDecrease}
            className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-l"
          >
            -
          </button>
          <button className=" font-bold py-2 px-4 rounded-r">
            {this.state.number}
          </button>
          <button
            onClick={this.handleIncrease}
            className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded-r"
          >
            +
          </button>
        </div>
        <div className="pt-4">
          <Nav />
        </div>
      </div>
    );
  }
  componentDidUpdate(preProps, preState) {
    // tự động được gọi sau khi render chạy xong
    console.log("old", preState);
    console.log("current", this.state);
    console.log("DidUpdate");
  }
}
