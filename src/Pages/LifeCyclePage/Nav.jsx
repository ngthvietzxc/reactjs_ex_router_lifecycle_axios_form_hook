import React, { PureComponent } from "react";

export default class Nav extends PureComponent {
  componentDidMount() {
    // dùng để gọi api khi user load trang
    console.log("Child-Didmount");
  }
  render() {
    console.log("Child Render");
    return (
      <div className="bg-green-700 p-4 text-black-800 mb-8 text-4xl font-bold sm:text-5xl md:mb-12 md:text-xl">
        Nav
      </div>
    );
  }
  componentWillUnmount() {
    console.log("Child - WillUnmount");
  }
}
