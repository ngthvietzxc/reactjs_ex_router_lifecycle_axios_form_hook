import axios from "axios";
import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export default class HomePage extends Component {
  state = {
    movieList: [],
  };
  componentDidMount() {
    axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP10",
      method: "GET",
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwNiIsIkhldEhhblN0cmluZyI6IjExLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDM5MDQwMDAwMCIsIm5iZiI6MTY3MDYwNTIwMCwiZXhwIjoxNjk0NTM4MDAwfQ.ndRbsF4IYaA6syfyNt9AVnZnm1kn9MAHUKKo4rXHLVE",
      },
    })
      .then((res) => {
        this.setState({ movieList: res.data.content });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  renderMovieList = () => {
    return this.state.movieList.map((item, index) => {
      return (
        <div
          className="max-w-sm rounded overflow-hidden shadow-lg col-3 mx-auto mb-5 "
          key={index}
        >
          <img
            className="w-full h-96 object-fit-cover pt-3"
            src={item.hinhAnh}
            alt="Sunset in the mountains"
          />
          <div className="px-6 py-4">
            <div className="font-bold text-xl mb-2">{item.tenPhim}</div>
            <p className="text-left text-gray-700 text-base pb-3">
              {item.moTa.length > 100
                ? item.moTa.slice(0, 100) + " . . . "
                : item.moTa}
              <a href="#">Đọc thêm</a>
            </p>
            <button className="bg-gray-400 hover:bg-white-500 text-white-700 font-semibold  py-2 px-4 border border-blue-500 hover:border-transparent rounded">
              <NavLink
                to={`/detail/${item.maPhim}`}
                style={{ textDecoration: "none" }}
              >
                Xem chi tiết
              </NavLink>
            </button>
          </div>
        </div>
      );
    });
  };
  render() {
    return (
      <div>
        <h1 className="p-4 text-pink-800 mb-8 text-4xl font-bold sm:text-5xl md:mb-12 md:text-6xl">
          Home Page
        </h1>
        <div className="row">{this.renderMovieList()}</div>
      </div>
    );
  }
}
