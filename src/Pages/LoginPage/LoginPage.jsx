import React, { Component } from "react";

export default class LoginPage extends Component {
  state = { username: "Alice", password: "" };
  handleOnChangeLogin = (event) => {
    console.log(event.target.name);
    let { value, name } = event.target;
    this.setState({ [name]: value });
  };
  render() {
    return (
      <div>
        <h1 className=" pt-4 text-black-800 mb-8 text-4xl font-bold sm:text-5xl md:mb-12 md:text-6xl">
          Login Page
        </h1>
        <div className="flex flex-col items-center justify-center w-screen  text-gray-700">
          <form className="flex flex-col bg-white rounded shadow-lg p-12">
            <label
              className="font-semibold text-xs text-left"
              htmlFor="usernameField"
            >
              Username
            </label>
            <input
              onChange={this.handleOnChangeLogin}
              value={this.state.username}
              className="flex items-center h-12 px-4 w-64 bg-gray-200 mt-2 rounded focus:outline-none focus:ring-2"
              type="text"
              name="username"
            />
            <label
              className="font-semibold text-xs mt-3 text-left"
              htmlFor="passwordField"
            >
              Password
            </label>
            <input
              onChange={this.handleOnChangeLogin}
              className="flex items-center h-12 px-4 w-64 bg-gray-200 mt-2 rounded focus:outline-none focus:ring-2"
              type="password"
              name="password"
            />
            <button
              type="button"
              className="flex items-center justify-center h-12 px-6 w-64 bg-blue-600 mt-8 rounded font-semibold text-sm text-blue-100 hover:bg-blue-700"
            >
              Login
            </button>
            <div className="flex mt-6 justify-center text-xs">
              <a className="text-blue-400 hover:text-blue-500" href="#">
                Forgot Password
              </a>
              <span className="mx-2 text-gray-300">/</span>
              <a className="text-blue-400 hover:text-blue-500" href="#">
                Sign Up
              </a>
            </div>
          </form>
          {/* Component End  */}
        </div>
      </div>
    );
  }
}
// dynamic key
let user = { name: "alice" };
user.name = "tomy";
user["name"] = "tomy";

let key = "age";
let value = 20;
key = "name";
value = "tomy";

user[key] = value;
