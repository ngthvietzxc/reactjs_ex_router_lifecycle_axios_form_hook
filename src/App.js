import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import Header from "./Pages/Component/Header/Header";
import LifeCyclePage from "./Pages/LifeCyclePage/LifeCyclePage";
import DetailPage from "./Pages/DetailPage/DetailPage";
import HookPage from "./Pages/HookPage/HookPage";
import TaiXiuPage from "./Pages/TaiXiuPage/TaiXiuPage";
import UseEffectPage from "./Pages/UseEffectPage/UseEffectPage";

function App() {
  return (
    <div className="App">
      <nav>
        <BrowserRouter>
          <Header />
          <Routes>
            <Route path="/home" element={<HomePage />} />
            <Route path="/login" element={<LoginPage />} />
            <Route path="/life-cycle" element={<LifeCyclePage />} />
            <Route path="/detail/:id" element={<DetailPage />} />
            <Route path="/learn-hook" element={<HookPage />} />
            <Route path="/tai-xiu" element={<TaiXiuPage />} />
            <Route path="/effect-page" element={<UseEffectPage />} />
          </Routes>
        </BrowserRouter>
      </nav>
    </div>
  );
}

export default App;
